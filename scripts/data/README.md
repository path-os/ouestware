## aiTerms.csv

Keywords to identify academic Artificial Intelligence papers produced by Gargiulo, F. et al. (2022) ‘A meso-scale cartography of the AI ecosystem’, (2004), pp. 1–14.

## OpenAlex_topic_mapping_table_final_topic_field_subfield_table.csv

OpenAlex Topic system table see: https://docs.openalex.org/api-entities/topics 