import { Client as ESClient, errors } from "@elastic/elasticsearch";
import {
  MappingProperty,
  MappingTypeMapping,
} from "@elastic/elasticsearch/lib/api/types";
import { wait } from "@ouestware/async";
import axios, { AxiosRequestConfig } from "axios";
import { flatten, identity, keyBy, keys, toPairs, uniq } from "lodash";

/**
 * Any change in this mapping must be ported in:
 * - logstash configuration
 *  */

interface IdDisplayName {
  id: string;
  display_name: string;
}
interface Apc {
  value: number;
  currency: string;
  value_usd: number;
  provenance: string;
}
export interface OpenAlexApiMetadata {
  doi: string;
  type: string;
  id: string;
  topics?: (IdDisplayName & {
    score: number;
    domain: IdDisplayName;
    field: IdDisplayName;
    subfield: IdDisplayName;
  })[];
  open_access?: {
    is_oa: boolean;
    oa_status: string;
    oa_url: string;
    any_repository_has_fulltext: boolean;
  };
  apc_list?: Apc;
  apc_paid?: Apc;
  cited_by_count?: number;
  cited_by_api_url?: string;
  fwci?: number;
  grants?: {
    funder: string;
    funder_display_name: string;
    award_id: string;
  }[];
  sustainable_development_goals?: (IdDisplayName & {
    score: number;
  })[];
}

const idDisplayNameMapping: Record<string, MappingProperty> = {
  id: { type: "keyword" },
  display_name: { type: "keyword" },
};
const apcMapping: Record<string, MappingProperty> = {
  value: { type: "float" },
  currency: { type: "keyword" },
  value_usd: { type: "float" },
  provenance: { type: "keyword" },
};

export const openAlexMappings: MappingTypeMapping = {
  properties: {
    doi: { type: "keyword" },
    type: { type: "keyword" },
    id: { type: "keyword" },
    open_access: {
      properties: {
        is_oa: { type: "boolean" },
        oa_status: { type: "keyword" },
        oa_url: { type: "keyword" },
        any_repository_has_fulltext: { type: "boolean" },
      },
    },
    topics: {
      properties: {
        ...idDisplayNameMapping,
        score: { type: "float" },
        domain: {
          properties: idDisplayNameMapping,
        },
        field: {
          properties: idDisplayNameMapping,
        },
        subfield: {
          properties: idDisplayNameMapping,
        },
      },
    },
    apc_list: { properties: apcMapping },
    apc_paid: { properties: apcMapping },
    cited_by_count: { type: "integer" },
    cited_by_api_url: { type: "keyword" },
    fwci: { type: "float" },
    grants: {
      properties: {
        funder: { type: "keyword" },
        funder_display_name: { type: "keyword" },
        award_id: { type: "keyword" },
      },
    },
    sustainable_development_goals: {
      properties: {
        ...idDisplayNameMapping,
        score: { type: "float" },
      },
    },
  },
};
export const doiPattern = /^10\.[0-9]{4,}\/[a-z0-9\-._: ;()/]+$/i;

export const retrieveOaMetaFromES = async (
  doi: string,
  client: ESClient,
  index: string
): Promise<OpenAlexApiMetadata | null> => {
  if (!doi.match(doiPattern)) {
    console.warn(`${doi} is not a valid DOI`);
    return null;
  }
  try {
    const openAlexMetadataResult = await client.get<OpenAlexApiMetadata>({
      index,
      id: doi,
    });
    if (openAlexMetadataResult.found && openAlexMetadataResult._source) {
      // return indexed info
      // console.log(`found ${openAlexMetadataResult._id} doi in ES`);
      return openAlexMetadataResult._source;
    }
  } catch (error) {
    if (error instanceof errors.ResponseError) {
      if (error.statusCode === 404) {
        return null;
      }
    }
    console.log(error);
  }
  return null;
};

export const addOpenAlexToCSVRecord = (
  record: Record<string, string>,
  oaMeta: OpenAlexApiMetadata | null = null
): Record<string, string> => {
  return {
    ...record,
    oa_id: oaMeta?.id || "",
    oa_type: oaMeta?.type || "",
    oa_open_access: oaMeta?.open_access?.oa_status || "",
    oa_domains:
      uniq(oaMeta?.topics?.map((t) => t?.domain?.display_name))
        .filter(identity)
        .join("|") || "",
    oa_fields:
      uniq(oaMeta?.topics?.map((t) => t?.field?.display_name)).join("|") || "",
    oa_subfields:
      uniq(oaMeta?.topics?.map((t) => t?.subfield?.display_name)).join("|") ||
      "",
    oa_apc_list: (oaMeta?.apc_list?.value_usd || "") + "",
    oa_apc_paid: (oaMeta?.apc_paid?.value_usd || "") + "",
    oa_cited_by_count: (oaMeta?.cited_by_count || "") + "",
    oa_cited_by_api_url: oaMeta?.cited_by_api_url || "",
    oa_fwci: (oaMeta?.fwci || "") + "",
    oa_funders:
      uniq(oaMeta?.grants?.map((g) => g.funder_display_name)).join("|") || "",
    oa_sustainable_development_goals:
      uniq(
        oaMeta?.sustainable_development_goals?.map((sdg) => sdg.display_name)
      ).join("|") || "",
  };
};

type Task = {
  doi: string;
  record: Record<string, string>;
};

export class OpenAlexQueryBuffer {
  //ES configuration
  client: ESClient;
  index: string;
  // OpenAlex API conf
  mailTo: string | null;
  timeout: number;

  // maximum size of the buffer before we drain it
  size: number;

  // we store a map of doi Promises which size will be this.size maximum
  private doiBuffer: Record<string, Record<string, string>[]> = {};

  nbTimedOut = 0;
  maxRetryTimeOut = 5;

  constructor(props: {
    client: ESClient;
    index: string;
    mailTo?: string;
    paralell: number;
    timeout?: number;
    nbDoiByQuery?: number;
  }) {
    this.client = props.client;
    this.index = props.index;
    this.size = props.nbDoiByQuery || 100;
    this.mailTo = props.mailTo || null;
    this.timeout = props.timeout || 10000;
    this.maxRetryTimeOut = props.paralell * 5;
  }

  private resetBuffer() {
    this.doiBuffer = {};
  }

  getBufferLength() {
    return keys(this.doiBuffer).length;
  }

  async queryOpenAlex(
    recordsToProcess: Record<string, Record<string, string>[]>
  ): Promise<null | Record<string, string>[]> {
    const dois = keys(recordsToProcess);
    try {
      console.log(`OpenAlex: API request with ${dois.length} Dois`);
      const params: AxiosRequestConfig["params"] = {
        // use polite/common pool
        mailto: this.mailTo || undefined,
        // use filter to list the doi we want to retrieve
        //filter=doi:https://doi.org/10.1371/journal.pone.0266781|https://doi.org/10.1371/journal.pone.0267149
        filter: `doi:${dois.join("|")}`,
        // use select to get only the needed field
        select: keys(openAlexMappings.properties).join(","),
        // make sure to retrieve the maximum amount of element
        "per-page": this.size,
      };
      const response = await axios.get<{ results: OpenAlexApiMetadata[] }>(
        "https://api.openalex.org/works",
        {
          params,
          timeout: this.timeout,
        }
      );
      // request succeed : reset timeout counter
      this.nbTimedOut = 0;
      // update cache with new results
      await this.client
        .bulk({
          index: this.index,
          operations: flatten(
            response.data.results.map((r) => [
              {
                index: {
                  _index: this.index,
                  _id: r.doi.replace("https://doi.org/", ""),
                },
              },
              r,
            ])
          ),
          refresh: "wait_for",
        })
        .catch((error) => {
          console.log(
            `OpenAlex: could not index results in cache ${
              this.index
            }: ${error.toJSON()}`
          );
          throw error;
        });

      const apiResultsByDoi = keyBy(response.data.results, (r) =>
        r.doi.replace("https://doi.org/", "")
      );
      // iterate through buffer to free pending task through their resolve method
      const results = flatten(
        toPairs(recordsToProcess).map(([doi, records]) => {
          const oaMeta = apiResultsByDoi[doi];
          if (oaMeta === undefined) {
            console.warn(`${doi} not found in OpenAlex`);
            //store it in ES to avoid reasking for it later
            // we don't need to wait for the result
            this.client.index({
              index: this.index,
              id: doi,
              document: { doi },
            });
          }
          return records.map((r) => addOpenAlexToCSVRecord(r, oaMeta));
        })
      );

      return results;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        if (error.response && error.response.status === 429) {
          // we reached a rate limit
          console.log("Rate limit received from OpenAlex");

          // delay resolution
          if (
            error.response &&
            error.response.headers &&
            error.response.headers["x-rateLimit-reset"]
          ) {
            const resetTime = error.response.headers["x-rateLimit-reset"];
            if (resetTime) {
              const resetDate = new Date(resetTime);
              const timeToReset = resetDate.getTime() - Date.now() + 1000;
              // schedule a retry
              // wait till reset time
              console.log(`Waiting ${timeToReset / 1000}s till ${resetDate}`);
              await wait(timeToReset);
              // retry
              console.log("retrying to query last paused buffer...");
              return await this.queryOpenAlex(recordsToProcess);
            }
          } else {
            console.log("Can't retrieve ratelimit_reset: quitting");
            console.log(JSON.stringify(error.response, null, 2));
            throw error;
          }
        } else if (error.code === "ECONNABORTED") {
          this.nbTimedOut += 1;
          if (this.nbTimedOut < this.maxRetryTimeOut) {
            console.log(
              `Request to openAlex timed-out, waiting ${
                this.timeout * this.nbTimedOut
              } before retry`
            );
            await wait(this.timeout);
            console.log("retry");
            return await this.queryOpenAlex(recordsToProcess);
          } else {
            console.log(`${this.nbTimedOut} timed-out occurred. Stopping...`);
            throw error;
          }
        } else {
          console.log(
            `Error ${error.status} from OpenAlex API with call ${error.message}` // ${JSON.stringify(error.toJSON(), null, 2)}`
          );
        }
      } else console.log(error + "");
      // stop the pipeline abruptly
      // TODO: should we abort the pipeline instead ?
      throw error;
    }
  }

  async drain(): Promise<null | Record<string, string>[]> {
    const recordsToProcess = { ...this.doiBuffer };
    this.resetBuffer();
    return await this.queryOpenAlex(recordsToProcess);
  }

  /**
   * process a doi task by adding it to the buffer and then processing the buffer at once
   * @param task
   * @returns result sent by API
   */
  async process(task: Task): Promise<Record<string, string>[] | null> {
    const { doi: currentDoi, record } = task;
    // doi already in buffer, store the record waiting for resolution
    if (this.doiBuffer[currentDoi] !== undefined) {
      this.doiBuffer[currentDoi].push(record);
      return null;
    }

    // add doi+record to the buffer
    this.doiBuffer[currentDoi] = [record];

    // test buffer size
    if (keys(this.doiBuffer).length === this.size) {
      // buffer is full, let's drain it

      return await this.drain();
    }
    return null;
  }
}
