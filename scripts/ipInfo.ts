import { IP, IPv4, IPv6 } from "ip-toolkit";
import IPinfoWrapper, { ApiLimitError } from "node-ipinfo";
import { GetIpMethod, IpRangeInfoType } from "./types";

export class IpInfo {
  token: string;
  ipinfoWrapper: IPinfoWrapper;

  constructor(token: string) {
    this.token = token;
    this.ipinfoWrapper = new IPinfoWrapper(token);
  }

  getIp: GetIpMethod = async (ip: string) => {
    if (!IP.isValidIP(ip)) {
      console.warn(`${ip} is not a valid IP address`);
      return null;
    }

    try {
      const response = await this.ipinfoWrapper.lookupIp(ip);
      if (!response.bogon) {
        const cidr =
          IPv4.parseCIDR(response.asn?.route || response.abuse?.network) ||
          IPv6.parseCIDR(response.asn?.route || response.abuse?.network);

        const ipRange: IpRangeInfoType = {
          range: {
            first_ip: cidr ? cidr.firstHost : ip,
            last_ip: cidr ? cidr.lastHost : ip,
            cidr: response.asn?.route || response.abuse?.network || `${ip}/32`,
            size: cidr ? Number(cidr.ipCount) : 1,
          },
          org: {
            name: response.company?.name || response.asn?.name,
            domain: response.company?.domain || response.asn?.domain,
            org_type: response.company?.type || response.asn?.type,
            // Note: geoloc is linked to IP not to org
            // potentially one iprange could have more than one geoloc but we are using only the first encountered one
            // but we may store more than one Ip range for the same org which can have different geoloc
            // Doing better would require to check geoloc for each single IP
            city: response.city,
            country: response.country,
            countryCode: response.countryCode,
            continent: response.continent.name,
            continentCode: response.continent.code,
            localisation: response.loc,
          },
          from: "api",
        };

        return ipRange;
      } else
        return {
          range: {
            first_ip: ip,
            last_ip: ip,
            cidr: `${ip}/32`,
            size: 1,
          },
          from: "api",
        };
    } catch (error) {
      console.warn(error);
      if (error instanceof ApiLimitError) {
        throw error;
      }
      return null;
    }
  };
}
