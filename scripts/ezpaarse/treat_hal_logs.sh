#!/bin/bash

# loop on file in tar
for tarFile in $(ls *.tar) 
do
    echo $tarFile
    for logFile in $(tar -tf $tarFile --wildcards '*.access_log.gz')
    do
        echo $logFile
        directory=$(dirname "$logFile")
        mkdir -p $directory
        filename=$(basename -- "$logFile")
        outputFilename="${filename%.access_log.gz}.ec.csv.gz"
        tar -xf $tarFile $logFile -O | gzip -cd | ezp process -H "ezPAARSE-Predefined-Settings:00-fr-hal-openalex" | gzip > $directory/$outputFilename
    done
done