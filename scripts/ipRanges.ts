import { Client as ESClient } from "@elastic/elasticsearch";
import {
  MappingTypeMapping,
  QueryDslQueryContainer,
} from "@elastic/elasticsearch/lib/api/types";
import { IP } from "ip-toolkit";

import { GetIpMethod, IpRangeInfoType } from "./types";

/**
 * Any change in this mapping must be ported in:
 * - types.ts
 * - logstash configuration
 *  */

export const ipRangeMappings: MappingTypeMapping = {
  properties: {
    range: {
      properties: {
        first_ip: {
          type: "ip",
        },
        last_ip: {
          type: "ip",
        },
        cidr: {
          type: "keyword",
        },
        size: {
          type: "long",
        },
      },
    },
    org: {
      properties: {
        // those keys will be prefixed with client_ in log index
        name: { type: "text" },
        org_type: {
          type: "keyword",
        },
        domain: {
          type: "keyword",
        },
        city: { type: "keyword" },
        country: { type: "keyword" },
        localisation: {
          type: "geo_point",
        },
      },
    },
    from: { type: "keyword" },
  },
};

export const retrieveIpRangeFromES = async (
  ip: string,
  client: ESClient,
  index: string = "ip_ranges"
): Promise<IpRangeInfoType | null> => {
  if (!IP.isValidIP(ip)) {
    console.warn(`${ip} is not a valid IP address`);
    return null;
  }
  // .0 and .255 special cases
  let _ip = ip;
  // for range filter to work we transform .0 to .1
  if (ip.endsWith(".0")) _ip = [...ip.split(".").slice(0, 3), "1"].join(".");
  // for range filter to work we transform .255 to .254
  if (ip.endsWith(".255"))
    _ip = [...ip.split(".").slice(0, 3), "254"].join(".");

  const query: QueryDslQueryContainer = {
    bool: {
      filter: [
        { range: { "range.first_ip": { lte: _ip } } },
        { range: { "range.last_ip": { gte: _ip } } },
      ],
    },
  };

  const rangeQueryResult = await client.search<IpRangeInfoType, undefined>({
    index,
    query,
    sort: { "range.size": "asc" },
  });
  const range =
    rangeQueryResult.hits.hits.length > 0 &&
    rangeQueryResult.hits.hits[0]._source != undefined
      ? rangeQueryResult.hits.hits[0]._source
      : null;
  if (range) {
    // return indexed info
    // console.log(`found ${ip} range in ES`);
    return { ...range, from: "es" };
  }
  return null;
};

export const retrieveIpRange = async (
  ip: string,
  getIp: GetIpMethod,
  client: ESClient,
  index: string = "ip_ranges"
): Promise<IpRangeInfoType | null> => {
  if (!IP.isValidIP(ip)) {
    console.warn(`${ip} is not a valid IP address`);
    return null;
  }

  // first check RANGE from ES
  const range = await retrieveIpRangeFromES(ip, client, index);
  if (range) {
    // return indexed info
    return range;
  } else {
    // retreive from method

    console.log(`asking ${ip} range to API`);
    const newRange = await getIp(ip);

    if (newRange !== null) {
      // store in index
      //console.log(`indexing range for ${ip}`);
      await client.index({
        index,
        id: newRange.range.cidr,
        document: newRange,
        refresh: "wait_for",
      });
      // return info
      return newRange;
    }
  }

  return null;
};
