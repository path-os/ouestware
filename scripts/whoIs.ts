import { IP, IPv4 } from 'ip-toolkit';
import {
  setTimeout,
} from 'node:timers/promises';
import { ip as whoiserIp } from "whoiser";

import { random } from "lodash";
import { GetIpMethod, IpRangeInfoType } from "./types";



export const getIpFromWhoIs: GetIpMethod = async (ip: string) => {


  if (!IP.isValidIP(ip)) {
    console.warn(`${ip} is not a valid IP address`);
    return null;
  }

  try {
    // gentle throttle
    await setTimeout(random(100, 1000));

    const response = (await whoiserIp(ip)) as Record<string, string>;

    // todo : check range format
    const [firstIp, lastIp] = response.range.split(' - ')
    const range = IPv4.ipRange.fromString(firstIp, lastIp)


    console.log(`retrieved ${response.range} ${response.route} from API`)
    const ipRange: IpRangeInfoType = {
      range: {
        first_ip: firstIp,
        last_ip: lastIp,
        cidr: response.route,
        size: range.ipCount(),
      },
      org: {
        name: response.netname,
        description: response.organisation
          ? (response.organisation as unknown as Record<string, string>)[
          "org-name"
          ]
          : response.descr,
      },
      from: "api",
    };

    return ipRange;
  } catch (error) {
    console.warn(error);
    return null;
  }
};
