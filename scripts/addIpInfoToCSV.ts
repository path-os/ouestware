import { Command, Option } from "@commander-js/extra-typings";
import { Client as ESClient } from "@elastic/elasticsearch";
import { parse } from "csv-parse";
import { stringify } from "csv-stringify";
import "dotenv/config";
import fs, { statSync } from "fs";
import { transform } from "stream-transform";
import zlib from "zlib";

import { mkdir } from "fs/promises";
import { fromPairs, last, mapKeys } from "lodash";
import { ApiLimitError } from "node-ipinfo";
import path from "path";
import { pipeline } from "stream/promises";
import { IpInfo } from "./ipInfo";
import {
  ipRangeMappings,
  retrieveIpRange,
  retrieveIpRangeFromES,
} from "./ipRanges";
import { GetIpMethod, IpRangeInfoType, ORG_FIELDNAMES } from "./types";
import { ensureESIndex } from "./utils";
import { getIpFromWhoIs } from "./whoIs";

const program = new Command()
  .name("add-ip-info-to-eezpaarse-log")
  .description(
    "stream a Eezpaarse gziped CSV log file and add Ip Owner information to it in output"
  )
  .requiredOption(
    "-i, --input <input>",
    "path to input file such as logs.csv.gz"
  )
  .requiredOption(
    "-o, --output <output>",
    "path to output file such as logs.csv.gz"
  )
  .option("-f, --force", "overwrite output if exists")
  .addOption(
    new Option("-a, --api <api>", "Which API to use")
      .choices(["cache-only", "ipInfo", "whoIs"])
      .default("whoIs")
  )
  .addOption(
    new Option(
      "-t, --ip-info-token <ipInfoToken>",
      "IpInfo API's token to use for authentication"
    ).env("IP_INFO_TOKEN")
  )
  .option(
    "-d, --delemiter <delimiter>",
    "CSV fields delimiter default to ';'",
    ";"
  )
  .addOption(
    new Option(
      "--ip-field <ipField>",
      "CSV field which contains IP information"
    ).default("client_ip")
  )
  .addOption(
    new Option(
      "--ip-ranges-index <ipRangesIndex>",
      "Name of the Elasticsearch Index to store and retrieve IP ranges"
    ).env("ELASTICSEARCH_IP_RANGES_INDEX")
    // don't allow to clear as it's dangerous
    // )
    // .addOption(
    //   new Option(
    //     "--clear-ip-ranges-index",
    //     "Clear the  IP ranges Elasticsearch Index before starting /!\ destructive"
    //   ).default(false)
  )
  .addOption(
    new Option(
      "-p, --parallel <parallel>",
      "Number of concurrent calls to throw to API at the same time."
    )
      .default(1)
      .argParser((o) => +o)
  );

program.parse();

const options = program.opts();
console.log(options);

// check input file existence
try {
  statSync(options.input);
} catch {
  console.error(`Input file ${options.input} can't be found`);
  process.exit(1);
}
if (!options.force) {
  // check output existence to avoid overwrite
  try {
    statSync(options.output);
    console.error(`Output file ${options.output} already exists`);
    process.exit(1);
  } catch {}
}

let getIp: GetIpMethod = getIpFromWhoIs;

const requiredEnvOptions: (keyof typeof options)[] = [
  "ipRangesIndex",
  "parallel",
];
requiredEnvOptions.forEach((option) => {
  if (options[option] === undefined) {
    console.log(
      `${option} must be provided either by cli option or through env variable`
    );
    process.exit(1);
  }
});

if (options.api === "ipInfo") {
  if (!options.ipInfoToken) {
    console.error(
      `Ip Info token is mandatory! Add a token in --token option or in  IP_INFO_TOKEN env variable`
    );
    process.exit(1);
  }
  const ipInfo = new IpInfo(options.ipInfoToken);
  getIp = ipInfo.getIp;
}

// init ES
const esClient = new ESClient({
  node: `http://${process.env.ELASTICSEARCH_HOST || "localhost"}:${
    process.env.ELASTICSEARCH_PORT || "9200"
  }`,
  auth:
    process.env.ELASTICSEARCH_USERNAME || process.env.ELASTICSEARCH_PASSWORD
      ? {
          username: process.env.ELASTICSEARCH_USERNAME || "elastic",
          password: process.env.ELASTICSEARCH_PASSWORD || "changeme",
        }
      : undefined,
});

let abort = false;
ensureESIndex(
  esClient,
  options.ipRangesIndex || "ipranges_ipinfo",
  ipRangeMappings,
  false
)
  .then(async () => {
    // streams
    const reader = fs.createReadStream(options.input);
    const gunzip = zlib.createGunzip({
      finishFlush: zlib.constants.Z_SYNC_FLUSH,
    });
    const parser = parse({ delimiter: options.delemiter, columns: true });
    const transformer = transform(
      {
        parallel: options.parallel,
      },
      async (record, write) => {
        const rawIp = record[options.ipField] || null;
        // sometimes client_ip sotres two different IP
        // we take the last one hopping that's the more precise one (i.e not ISP)
        //TODO: check with ezzzparse team about this
        const ip = rawIp
          ? rawIp.includes(", ")
            ? last(rawIp.split(", "))
            : rawIp
          : "";
        let range: IpRangeInfoType | null = null;

        if (!abort && options.api !== "cache-only") {
          try {
            range = await retrieveIpRange(
              ip,
              getIp,
              esClient,
              options.ipRangesIndex
            );
          } catch (error) {
            // STOP stream chain on rate limit error
            if (error instanceof ApiLimitError) {
              console.log(`API rate limit, aborting...`);
              abort = true;
            }
          }
        } else {
          // API not reachable, look up cache only
          range = await retrieveIpRangeFromES(
            ip,
            esClient,
            options.ipRangesIndex
          );
        }
        // build the new record adding range org info
        if (range)
          write(null, {
            ...record,
            ...fromPairs(ORG_FIELDNAMES.map((f) => [`client_${f}`, ""])),
            ...mapKeys(range.org, (_, k) => `client_${k}`),
            client_cidr_from: range.from,
            client_cidr: range.range.cidr,
          });
        // making sure to output the right columns for data even if empty as the very frist record set the file structure
        // todo: discover headers from the parser and declare columns in the stringifier
        else
          write(null, {
            ...record,
            ...fromPairs(ORG_FIELDNAMES.map((f) => [`client_${f}`, ""])),
            client_cidr_from: "",
            client_cidr: "",
          });
        return;
      }
    );
    transformer.on("error", function (err) {
      console.error(err.message);
    });
    const stringifier = stringify({
      delimiter: options.delemiter,
      header: true,
    });
    // will add csv and transform
    const gzip = zlib.createGzip();
    await mkdir(path.dirname(options.output), { recursive: true });
    const writer = fs.createWriteStream(options.output);
    transformer.setMaxListeners(Math.max(options.parallel + 3, 10));
    let sigIntCount = 0;
    process.on("SIGINT", function () {
      sigIntCount += 1;
      console.log(
        "Caught interrupt signal: aborting treatment, finishing writing file..."
      );
      // abort the pipeline
      abort = true;
      // second SIGINT don't wait for end of pipeline
      if (sigIntCount === 2) {
        console.log("Second SIGNINT => forcing to exit");
        process.exit(1);
      }
    });

    await pipeline(
      reader,
      gunzip,
      parser,
      transformer,
      stringifier,
      gzip,
      writer
    );
  })
  .catch((error) => console.error(error));
