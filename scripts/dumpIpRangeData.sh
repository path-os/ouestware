#!/bin/bash

source .env;
node_modules/elasticdump/bin/elasticdump --input=http://${ELASTICSEARCH_HOST}:${ELASTICSEARCH_PORT}/${ELASTICSEARCH_IP_RANGES_INDEX} --output=/data/pathos/ipRanges/ipRanges_$(date -d 'today' +'%Y%m%d_%H%M').ndjson.gz --limit 1000 --fsCompress --type=data