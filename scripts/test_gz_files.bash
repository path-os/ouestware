#!/bin/bash

for file in $@;
do
	if gzip -t $file &>/dev/null; then
		true
	else
		printf "$file\n";
	fi
done

