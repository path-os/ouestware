import { Client as ESClient } from "@elastic/elasticsearch";
import { MappingTypeMapping } from "@elastic/elasticsearch/lib/api/types";

export const ensureESIndex = async (
  client: ESClient,
  indexName: string,
  mappings: MappingTypeMapping,
  clear?: boolean
) => {
  const exists = await client.indices.exists({ index: indexName });
  if (clear && exists) await client.indices.delete({ index: indexName });

  if (!exists || clear) {
    await client.indices.create({
      index: indexName,
      mappings,
    });
  }
};
