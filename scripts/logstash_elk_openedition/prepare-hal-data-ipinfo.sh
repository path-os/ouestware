#!/bin/bash
set -eou pipefail

#
# Go to the folder where the script is located
#
SCRIPT_PATH="/data/pathos/ow_code/scripts"
SCRIPT_NAME=addIpInfoToCSV
cd $SCRIPT_PATH

#PLATEFORMS=("books" "catOEJ" "journals" "search")
YEAR=2024
for month in /data/pathos/hal/ec_with_openalex/$YEAR/*; 
do
    mkdir -p /data/pathos/hal/ec_with_openalex_ipinfo/$YEAR/$(basename $month)
    for inputFile in $month/*.csv.gz; 
    do
      outputFile=/data/pathos/hal/ec_with_openalex_ipinfo/$YEAR/$(basename $month)/$(basename $inputFile)
      if [ ! -f $outputFile ]; then
        echo "*************************************************************"
        echo "PREPARING $inputFile into $outputFile"
        echo "*************************************************************"
        time npm run $SCRIPT_NAME -- -i $inputFile  -o $outputFile -p 10 -a ipInfo --ip-field host -f || true
        echo
      fi
    done
done
