#!/bin/bash
set -eou pipefail

#
# Go to the folder where the script is located
#
SCRIPT_PATH="/data/pathos/ow_code/scripts"
SCRIPT_NAME=addOpenAlexToCSV #addIpInfoToCSV
cd $SCRIPT_PATH

#PLATEFORMS=("books" "catOEJ" "journals" "search")
FILES_TO_REDO=("2023-10/journals-10182023.ec.csv.gz")
for file in "${FILES_TO_REDO[@]}"
do
      echo "*************************************************************"
      echo "PREPARING $file"
      echo "*************************************************************"
      time npm run $SCRIPT_NAME -- -i /data/pathos/ec/journals/$file  -o /data/pathos/ec_with_openalex/journals/$file -p 6 -t 20000 -f || true
      echo 
done
