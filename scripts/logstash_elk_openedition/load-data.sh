#!/bin/bash
set -eou pipefail

#
# Go to the folder where the script is located
#
SCRIPT_PATH="`dirname \"$0\"`"
cd $SCRIPT_PATH

#PLATEFORMS=("books" "catOEJ" "journals" "search")
PLATEFORMS=("journals")

for PF in "${PLATEFORMS[@]}"
do 
  days=("31")
  for i in "${days[@]}" 
  do
  #for i in {02..31}; do
    echo "*************************************************************"
    echo "IMPORTING $PF DAY $i"
    echo "*************************************************************"
    ./logstash_create_conf_and_run.sh $PF $i 
    echo 
  done
done
