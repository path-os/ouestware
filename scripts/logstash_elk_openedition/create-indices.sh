#!/bin/bash
set -eou pipefail

ELK="http://localhost:9200"

for pf in 'oej' 'oeb' 'oes'
do
    echo -e "\ncréation de l'index ${pf} "
    curl -X DELETE "${ELK}/${pf}-ec" 
    curl -XPUT "${ELK}/${pf}-ec?pretty" -H 'Content-Type: application/json' -d@model.json
done

