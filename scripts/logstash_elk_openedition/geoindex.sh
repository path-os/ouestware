#!/bin/bash
set -eou pipefail

ELK="http://localhost:9200"

for pf in 'oej' 'oeb' 'oes'
do
  curl -X POST "${ELK}/${pf}-ec/_update_by_query" -H 'Content-Type: application/json' -d'
{
  "script": {
    "inline": "ctx._source[\"geoip-point\"] = [\"lat\": ctx._source[\"geoip-latitude\"], \"lon\": ctx._source[\"geoip-longitude\"]]",
    "lang": "painless"
  },
  "query": {
    "bool": {
      "filter": [
        {
          "exists": { 
            "field": "geoip-latitude"
          }
        },
        {
          "exists": { 
            "field": "geoip-longitude"
          }
        }
      ]
    }
  }
}'
done

