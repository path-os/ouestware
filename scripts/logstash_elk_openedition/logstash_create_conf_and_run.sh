#!/bin/bash
# génération du fichier quotidien de configuration pour logstash

warn () {
    echo "$0:" "$@" >&2
}
die () {
    rc=$1
    shift
    warn "$@"
    exit $rc
}

pf=${1};
day=${2}
if [ -z ${pf} ] ; then
  die 0 "plateforme manquante (journals books catOEJ search)"
fi
if [ -z ${day} ] ; then
  die 0 "day is missing"
fi
year=2022
month=10
ipELK="localhost:9200"

if [ ${pf} = "books" ]; then
    platform="oeb"
    elif [ ${pf} = "journals" ]; then
    platform="oej"
    elif [ ${pf} = "catOEJ" ]; then
    platform="oej"
    else
    platform="oes"
    fi
SCRIPT_PATH="`dirname \"$0\"`"
confFile="/data/pathos/ec_with_openalex_ipinfo/${pf}-${year}${month}${day}.conf"
resultFile="/data/pathos/ec_with_openalex_ipinfo/${pf}/${year}-${month}/${pf}-${month}${day}${year}.ec.csv.gz"
tempFile="/tmp/${month}${day}${year}.ec.csv.gz"

cp ${resultFile} ${tempFile}
echo "Fichier de configuration logstash : ${confFile}"
echo "Fichier de données : ${resultFile}"


input="
input {
    file {
        path => \"${tempFile}\"
        start_position => \"beginning\"
        mode => "read"
        exit_after_read => true
	sincedb_path => \"/dev/null\"
    }
}
"

output="
output {
    elasticsearch {
        hosts => [\"http://${ipELK}\"]
        action => \"index\"
        index => \"${platform}-ec-ipinfo-openalex\"
    }
    stdout { }
} 
"

echo "${input}" > ${confFile}
cat ${SCRIPT_PATH}/logstash_filter.txt >> ${confFile}
echo "${output}" >> ${confFile}
echo "" 

# lancement de logstash
sudo -u logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash --path.config=${confFile} --log.level=warn
