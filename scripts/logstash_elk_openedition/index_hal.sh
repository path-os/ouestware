#!/bin/bash
ELK="http://localhost:9200"
index="hal-ec-ipinfo-openalex"
exists=$(curl -s -o /dev/null -w "%{http_code}" --head http://localhost:9200/$index)
if [ $exists = "404" ]
then
	echo -e "\ncréation de l'index ${index}";
	curl -XPUT "${ELK}/${index}?pretty" -H 'Content-Type: application/json' -d@model.json;
fi
echo "starting logstash"
xan glob "/data/pathos/hal/ec_with_openalex_ipinfo/2024/0[5,6,7,8,9]/*.ec.csv.gz" | xan cat rows path --input "-" -d ";" | sudo -u logstash /usr/share/logstash/bin/logstash -w 10 --path.settings=/etc/logstash --path.config=$(pwd)/hal.logstash.conf --log.level=warn
