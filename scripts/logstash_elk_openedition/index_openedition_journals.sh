#!/bin/bash
ELK="http://localhost:9200"
index="oej-ec-ipinfo-openalex"
exists=$(curl -s -o /dev/null -w "%{http_code}" --head http://localhost:9200/$index)
if [ $exists = "404" ]
then
	echo -e "\ncréation de l'index ${index}";
	curl -XPUT "${ELK}/${index}?pretty" -H 'Content-Type: application/json' -d@model.json;
fi
echo "starting logstash"
xan merge -d ";" /data/pathos/ec_with_openalex_ipinfo/journals/2023-0*/*.ec.csv.gz | xan cat rows | sudo -u logstash /usr/share/logstash/bin/logstash -w 10 --path.settings=/etc/logstash --path.config=$(pwd)/open_edition_journals.logstash.conf --log.level=error
