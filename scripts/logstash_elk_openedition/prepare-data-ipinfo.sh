#!/bin/bash
set -eou pipefail

#
# Go to the folder where the script is located
#
SCRIPT_PATH="/data/pathos/ow_code/scripts"
SCRIPT_NAME=addIpInfoToCSV
cd $SCRIPT_PATH

#PLATEFORMS=("books" "catOEJ" "journals" "search")
PLATEFORMS=("journals")
for PF in "${PLATEFORMS[@]}"
do 

  for month in /data/pathos/ec_with_openalex/$PF/2023*; 
  do
    mkdir -p /data/pathos/ec_with_openalex_ipinfo/$PF/$(basename $month)
    for inputFile in $month/*.csv.gz; 
    do
      outputFile=/data/pathos/ec_with_openalex_ipinfo/$PF/$(basename $month)/$(basename $inputFile)
      if [ ! -f $outputFile ]; then
        echo "*************************************************************"
        echo "PREPARING $inputFile into $outputFile"
        echo "*************************************************************"
        time npm run $SCRIPT_NAME -- -i $inputFile  -o $outputFile -p 10 -a ipInfo || true
        echo
      fi
    done
  done
done
