#!/bin/bash
set -eou pipefail

#
# Go to the folder where the script is located
#
SCRIPT_PATH="/data/pathos/ow_code/scripts"
SCRIPT_NAME=addOpenAlexToCSV #addIpInfoToCSV
cd $SCRIPT_PATH

#PLATEFORMS=("books" "catOEJ" "journals" "search")
PLATEFORMS=("journals")
for PF in "${PLATEFORMS[@]}"
do 

  for month in /data/pathos/ec/$PF/2023*; 
  do
    mkdir -p /data/pathos/ec_with_openalex/$PF/$(basename $month)
    for inputFile in $month/*.csv.gz; 
    do
      echo "*************************************************************"
      echo "PREPARING $inputFile"
      echo "*************************************************************"
      time npm run $SCRIPT_NAME -- -i $inputFile  -o /data/pathos/ec_with_openalex/$PF/$(basename $month)/$(basename $inputFile) -p 6 -t 20000 || true
      echo 
    done
  done
done
