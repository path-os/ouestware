import { parse, stringify } from "csv/sync";
import { readFileSync, writeFileSync } from "fs";
import { sortBy } from "lodash";

interface OpenAlexTopicType {
  topic_id: number;
  topic_name: string;
  subfield_id: number;
  subfield_name: string;
  field_id: number;
  field_name: string;
  domain_id: number;
  domain_name: string;
  keywords: string[];
  summary: string;
  wikipedia_url: string;
  nbAiMatches?: number;
  keywordsAiRatio?: number;
}

// read OpenAlex CSV
const openAlexTopicsCSV = readFileSync(
  "./data/OpenAlex_topic_mapping_table_final_topic_field_subfield_table.csv"
);
const records = parse(openAlexTopicsCSV, {
  columns: true,
  skip_empty_lines: true,
  trim: true,
  cast: (value, context) => {
    if (context.column === "keywords")
      return value.split("; ").map((k) => k.toLocaleLowerCase().trim());
    return value;
  },
}) as OpenAlexTopicType[];

// read tags
const aiKeywords = new Set(
  readFileSync("./data/aiTerms.csv")
    .toString()
    .split("\n")
    .map((k) => k.trim())
);

// for each topics
const aiRecords = records.map((topic) => {
  // how many keyword are in aiKeyword list
  // TODO: fuzzy matching
  const nbAiMatches = topic.keywords.filter((k) => aiKeywords.has(k)).length;

  return {
    ...topic,
    nbAiMatches,
    keywordsAiRatio: nbAiMatches / topic.keywords.length,
  };
});

// write result
writeFileSync(
  "./data/OpenAlex_topic_mapping_table_final_topic_field_subfield_table_ai.csv",
  stringify(
    sortBy(
      aiRecords.filter((r) => r.nbAiMatches > 0),
      (r) => r.keywordsAiRatio * -1
    ),
    {
      header: true,
      cast: {
        object: (value, context) => {
          if (context.column === "keywords" && Array.isArray(value))
            return value.join("; ");
          return JSON.stringify(value);
        },
      },
    }
  )
);
