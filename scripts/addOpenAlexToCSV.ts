import { Command, Option } from "@commander-js/extra-typings";
import { Client as ESClient } from "@elastic/elasticsearch";
import { parse } from "csv-parse";
import { stringify } from "csv-stringify";
import "dotenv/config";
import fs, { statSync } from "fs";
import { transform } from "stream-transform";
import zlib from "zlib";

import { serial, wait } from "@ouestware/async";
import { mkdir } from "fs/promises";
import path from "path";
import { pipeline } from "stream/promises";
import {
  addOpenAlexToCSVRecord,
  doiPattern,
  openAlexMappings,
  OpenAlexQueryBuffer,
  retrieveOaMetaFromES,
} from "./openAlex";
import { ensureESIndex } from "./utils";

const program = new Command()
  .name("add-openalex-to-eezpaarse-log")
  .description(
    "stream a Eezpaarse gziped CSV log file and add Open Alex information to it in output"
  )
  .requiredOption(
    "-i, --input <input>",
    "path to input file such as logs.csv.gz"
  )
  .requiredOption(
    "-o, --output <output>",
    "path to output file such as logs.csv.gz"
  )
  .option("-f, --force", "overwrite output if exists")
  .option("-c, --cache-only", "Don't use API calls, only use local cache data.")
  .option(
    "-d, --delemiter <delimiter>",
    "CSV fields delimiter default to ';'",
    ";"
  )
  .addOption(
    new Option(
      "-m, --mailto <mailto>",
      "email address used to authenticate OpenAlex API call in order to use the polite pool"
    ).env("OPENALEX_MAILTO")
  )
  .addOption(
    new Option(
      "--cache-index <cacheIndex>",
      "Name of the Elasticsearch Index to store and retrieve OpenAlex document"
    ).env("ELASTICSEARCH_OPENALEX_CACHE_INDEX")
  )
  .addOption(
    new Option(
      "-n, --nbDocByQuery <nbDocByQuery>",
      "Number of DOIs to ask by API request (max 100)."
    )
      .default(100)
      .argParser((o) => +o)
  )
  .addOption(
    new Option(
      "-p, --parallel <parallel>",
      "Number of concurrent calls to throw to API at the same time."
    )
      .default(2)
      .argParser((o) => +o)
  )
  .addOption(
    new Option(
      "-t, --timeout <timeout>",
      "OpenAlex API Timeout in ms: time to wait for an answer before considering the request failed"
    )
      .default(10000)
      .argParser((o) => +o)
  );

program.parse();

const options = program.opts();
console.log(options);
const requiredEnvOptions: (keyof typeof options)[] = ["cacheIndex", "mailto"];
requiredEnvOptions.forEach((option) => {
  if (options[option] === undefined) {
    console.log(
      `${option} must be provided either by cli option or through env variable`
    );
    process.exit(1);
  }
});

// check input file existence
try {
  statSync(options.input);
} catch {
  console.error(`Input file ${options.input} can't be found`);
  process.exit(1);
}
if (!options.force) {
  // check output existence to avoid overwrite
  try {
    statSync(options.output);
    console.error(`Output file ${options.output} already exists`);
    process.exit(1);
  } catch {}
}

// init ES
const esClient = new ESClient({
  node: `http://${process.env.ELASTICSEARCH_HOST || "localhost"}:${
    process.env.ELASTICSEARCH_PORT || "9200"
  }`,
  auth:
    process.env.ELASTICSEARCH_USERNAME || process.env.ELASTICSEARCH_PASSWORD
      ? {
          username: process.env.ELASTICSEARCH_USERNAME || "elastic",
          password: process.env.ELASTICSEARCH_PASSWORD || "changeme",
        }
      : undefined,
});

//init queue

const buffer = new OpenAlexQueryBuffer({
  client: esClient,
  index: options.cacheIndex || "OpenAlex",
  mailTo: options.mailto,
  timeout: options.timeout,
  nbDoiByQuery: options.nbDocByQuery,
  paralell: options.parallel,
});

// aborting: we use a variable to abort processing data
let abort = false;
ensureESIndex(
  esClient,
  options.cacheIndex || "OpenAlex",
  openAlexMappings,
  false
)
  .then(async () => {
    // streams
    const reader = fs.createReadStream(options.input);
    const gunzip = zlib.createGunzip({
      finishFlush: zlib.constants.Z_SYNC_FLUSH,
    });
    const parser = parse({ delimiter: options.delemiter, columns: true });
    parser.on("end", async () => {
      console.log("Parser ended");
    });

    const transformer = transform(
      {
        parallel: options.parallel,
      },
      async (record, write) => {
        const doi = record.doi;
        if (abort || doi === "") {
          write(null, addOpenAlexToCSVRecord(record));
          return;
        }
        if (!doi.match(doiPattern)) {
          console.warn(`${doi} is not a valid DOI`);
          write(null, addOpenAlexToCSVRecord(record));
          return;
        }
        let oaMeta = await retrieveOaMetaFromES(
          doi,
          esClient,
          options.cacheIndex || "OpenAlex"
        );

        if (oaMeta) {
          write(null, addOpenAlexToCSVRecord(record, oaMeta));
          return;
        }

        if (!abort && !options.cacheOnly && oaMeta === null) {
          // ask API
          const results = await buffer.process({ doi, record });
          if (results !== null) {
            write(null, ...results);
          } else write(null, null);
          return;
        }
        // } catch (error: any) {
        //   console.log(
        //     `could not process record ${JSON.stringify(
        //       error.toJSON(),
        //       null,
        //       2
        //     )}`
        //   );
        // }
        write(null, null);
        return;
      }
    );

    //allow many listeners to handle parallelism
    transformer.setMaxListeners(options.parallel + 1);
    transformer.on("error", function (err) {
      console.error(err.message);
    });

    const stringifier = stringify({
      delimiter: options.delemiter,
      header: true,
    });

    // make sure to drain the buffer and write final data after finishing consuming the upstream readable but before closing
    transformer._final = async (cb) => {
      // wait for next tick to make sure last async paralell reads are processed
      await wait(0);
      if (!abort && buffer.getBufferLength() > 0) {
        console.log("Transfomer ended, let's drain API calls buffer");
        while (buffer.getBufferLength() > 0) {
          // if some records are still in the buffer
          const results = await buffer.drain();
          if (results !== null) {
            console.log(`writing the last ${results.length} records...`);
            //write the last records one by one in series respecting downstream pace
            await serial(
              results.map((r) => () => {
                return new Promise<void>((resolve, reject) => {
                  stringifier.write(r, undefined, (err) => {
                    if (err) reject(err);
                    else resolve();
                  });
                });
              })
            );
          }
        }
      }
      cb();
    };

    // will add csv and transform
    const gzip = zlib.createGzip();
    await mkdir(path.dirname(options.output), { recursive: true });
    const writer = fs.createWriteStream(options.output);
    let sigIntCount = 0;
    process.on("SIGINT", function () {
      sigIntCount += 1;
      console.log(
        "Caught interrupt signal: aborting treatment, finishing writing file..."
      );
      // abort the pipeline
      abort = true;
      // second SIGINT don't wait for end of pipeline
      if (sigIntCount === 2) {
        console.log("Second SIGNINT => forcing to exit");
        process.exit(1);
      }
    });

    try {
      await pipeline(
        reader,
        gunzip,
        parser,
        transformer,
        stringifier,
        gzip,
        writer
      ).catch((error) => console.log(error));
      console.log("pipeline finished");
    } catch (error) {
      console.log("error in pipeline");
      console.log(error);
    }
  })
  .catch((error) => console.error(error));
