// Order is important as it is used to order columns in CSV output which is important for logstash
export const ORG_FIELDNAMES = [
  "name",
  "domain",
  "org_type",
  "city",
  "country",
  "countryCode",
  "continent",
  "continentCode",
  "localisation",
] as const;

export interface IpRangeInfoType {
  range: {
    first_ip: string;
    last_ip: string;
    size: number;
    cidr: string;
  };
  org: Record<(typeof ORG_FIELDNAMES)[number], string>;
  from: "api" | "es";
}

export type GetIpMethod = (ip: string) => Promise<IpRangeInfoType | null>;
