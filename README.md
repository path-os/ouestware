# PATHos

This repository contain code developed to treat web access logs from two major Open Science French platforms: HAL and Open Edition.

Our goal being to study the consumptions of open science we used external data sources to enhance raw logs with:
- Open Access Mode: provided by Open Alex 
- IP address owner information provided by IpInfo

We collected more than just the open access variable from Open Alex:
- oa_id: OpenAlex identifier
- oa_type: OpenAlex publication type
- oa_open_access: OpenAlex Open Access mode
- oa_domains: OpenAlex Topic level 1
- oa_fields: OpenAlex Topic level 2
- oa_subfields: OpenAlex Topic level 3
- oa_apc_list: APC amount required by the publisher in US$
- oa_apc_paid: APC amount paid by the author in US$
- oa_cited_by_count: number of citation according to Open Alex
- oa_cited_by_api_url: list of citations according to Open Alex
- oa_fwci: OpenAlex metric
- oa_funders: Research funders
- oa_sustainable_development_goals: see OpenAlex doc

IpInfo provide information about the Ip ranges rented by organizations.
For each Ip we know what's its range and which organization controls it.
IpInfo also provide a category classifying organizations among 5 values: 
- ISP: Internet services providers 
- hosting: Cloud services providers
- business: private organizations
- education: universities, research institutions...
- government: governmental bodies

We also collected organization name, address and geolocalization.

## Open Edition Journals

As of 2025-01-21, only the Open edition Journals logs were treated from 2023-01-01 to 2023-12-31.
Logs were given under the ec.csv format i.e. after being treated by ezpaarse (TODO: archive ezpaarse configuration).

From the ezpaarse outputs we run two scripts provided in this repository: 

- `npm run addOpenAlexToCSV` to collect OpenAlex information from the DOI provided in the log
- `npm run addIpInfoToCSV` to collect IpInfo API information about the IP indicated in the log

Those scripts streams input file collect data with a cache implemented in ElasticSearch and stream the output in a csv.gz file.

Once the treatment were done, we used a [third script](./scripts/logstash_elk_openedition/index_openedition_journals.sh) to index the logs into elasticsearch using logstash.

## HAL

HAL provided access logs from 2023-09-01 till 2024-09-08. 
Those logs are raw logs as their own ezpaarse treatment remove the IP which we need for this research. Therefore we've used ezpaarse to treat the raw logs. We used a newly created OpenAlex ezpaarse plugin which does the same collect as the script used for Open Edition but into the ezpaarse ecosystem using the mongo cache.
We haven't port our IpInfo script in to a plugin so we had to run the IpInfo collect using the same script as Open Edition on ezpaarse output.

Once done we indexed the logs into Elasticsearch using logstash with [this script](./scripts/logstash_elk_openedition/index_hal.sh)

## ElasticSearch Mapping

Both logs uses the same index mapping. Some fields are this beeing said platform specific.
Refer to the [common mapping](./scripts//logstash_elk_openedition/model.json) adn the specific logstash configurations for [open edition](./scripts/logstash_elk_openedition//open_edition_journals.logstash.conf) and [hal](./scripts/logstash_elk_openedition/hal.logstash.conf).

